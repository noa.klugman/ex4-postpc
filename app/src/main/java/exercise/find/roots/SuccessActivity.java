package exercise.find.roots;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class SuccessActivity extends Activity {

    String originalNumber = "";
    String root1 = "";
    String root2 ="";
    String time="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success);

        TextView outcome = findViewById(R.id.outcome);
        TextView timeText = findViewById(R.id.timeText);

        Intent incomingIntent = getIntent();
        originalNumber = String.valueOf(incomingIntent.getLongExtra("original_number", -1));
        root1 = String.valueOf(incomingIntent.getLongExtra("root1", -1));
        root2 = String.valueOf(incomingIntent.getLongExtra("root2", -1));
        time = String.valueOf(incomingIntent.getDoubleExtra("time", -1));


        outcome.setText(originalNumber + " = " + root1 + " * " + root2);
        timeText.setText("time of calculation: " + time + " seconds");


    }

}