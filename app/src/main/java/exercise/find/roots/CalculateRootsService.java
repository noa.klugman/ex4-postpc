package exercise.find.roots;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class CalculateRootsService extends IntentService {


  public CalculateRootsService() {
    super("CalculateRootsService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent == null) return;
    long timeStartMs = System.currentTimeMillis();
    long numberToCalculateRootsFor = intent.getLongExtra("number_for_service", 0);
    if (numberToCalculateRootsFor <= 0) {
      Log.e("CalculateRootsService", "can't calculate roots for non-positive input" + numberToCalculateRootsFor);
      return;
    }
    /*
    TODO:
     calculate the roots.
     check the time (using `System.currentTimeMillis()`) and stop calculations if can't find an answer after 20 seconds
     upon success (found a root, or found that the input number is prime):
      send broadcast with action "found_roots" and with extras:
       - "original_number"(long)
       - "root1"(long)
       - "root2"(long)
     upon failure (giving up after 20 seconds without an answer):
      send broadcast with action "stopped_calculations" and with extras:
       - "original_number"(long)
       - "time_until_give_up_seconds"(long) the time we tried calculating

      examples:
       for input "33", roots are (3, 11)
       for input "30", roots can be (3, 10) or (2, 15) or other options
       for input "17", roots are (17, 1)
       for input "829851628752296034247307144300617649465159", after 20 seconds give up

     */


  /*  boolean done = false;
    for(long i = 2; i <= numberToCalculateRootsFor/2 && !done; ++i){
      timeStartMs = System.currentTimeMillis();
      if(timeStartMs >= 20_000L){ //20sec = 20_000 millisec
        Intent unsuccessBroadCast = new Intent("stopped_calculations");
        unsuccessBroadCast.putExtra("original_number", numberToCalculateRootsFor);
        unsuccessBroadCast.putExtra("time_until_give_up_seconds", timeStartMs/1000);
        sendBroadcast(unsuccessBroadCast);
        done = true;
      }
      else{
        if(numberToCalculateRootsFor % i == 0){
          Intent successBroadCast = new Intent("found_roots");
          successBroadCast.putExtra("original_number", numberToCalculateRootsFor);
          successBroadCast.putExtra("root1", i);
          successBroadCast.putExtra("root2", numberToCalculateRootsFor/i);
          successBroadCast.putExtra("time", timeStartMs/1000L);
          sendBroadcast(successBroadCast);
          done = true;
        }
      }
    }
    if(!done){
      Intent primeBroadCast = new Intent("found_roots");
      primeBroadCast.putExtra("original_number", numberToCalculateRootsFor);
      primeBroadCast.putExtra("root1", numberToCalculateRootsFor);
      primeBroadCast.putExtra("root2", 1);
      primeBroadCast.putExtra("time", timeStartMs/1000L);
      sendBroadcast(primeBroadCast);
    }*/

    boolean found = false;
    long root = 2;
    long initTime = timeStartMs;
    while (timeStartMs-initTime <= 20_000L && root <= numberToCalculateRootsFor && !found){

      if(numberToCalculateRootsFor % root == 0){
        Intent successBroadCast = new Intent("found_roots");
        successBroadCast.putExtra("original_number", numberToCalculateRootsFor);
        successBroadCast.putExtra("root1", root);
        successBroadCast.putExtra("root2", numberToCalculateRootsFor/root);
        successBroadCast.putExtra("time", (double)(timeStartMs-initTime)/1000);
        sendBroadcast(successBroadCast);
        found = true;
      }
      timeStartMs = System.currentTimeMillis();
      root++;
    }
    //if we found a root, now root = number+2, else - it's not and the time was ended before we found a root

    if(!found){
      Intent unsuccessBroadCast = new Intent("stopped_calculations");
      unsuccessBroadCast.putExtra("original_number", numberToCalculateRootsFor);
      unsuccessBroadCast.putExtra("time_until_give_up_seconds", (double)(timeStartMs-initTime)/1000);
      sendBroadcast(unsuccessBroadCast);
    }
  }
}